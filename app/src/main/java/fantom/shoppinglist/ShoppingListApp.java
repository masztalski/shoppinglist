package fantom.shoppinglist;

import android.app.Application;

import fantom.shoppinglist.di.ApplicationComponent;
import fantom.shoppinglist.di.DaggerApplicationComponent;
import fantom.shoppinglist.di.modules.ApplicationModule;


public class ShoppingListApp extends Application {
    public ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }
}
