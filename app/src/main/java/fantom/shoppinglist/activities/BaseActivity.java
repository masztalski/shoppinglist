package fantom.shoppinglist.activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

/**
 * Base activity used to setup toolbar
 */
public abstract class BaseActivity extends AppCompatActivity {

    public void setupToolbar() {
        ActionBar actionBar = setup(prepareToolbar());
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(shouldDisplayHomeAsUpEnabled());
        }
    }

    private ActionBar setup(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        return getSupportActionBar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(shouldDisplayHomeAsUpEnabled() && item.getItemId() == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public abstract Toolbar prepareToolbar();
    public abstract boolean shouldDisplayHomeAsUpEnabled();
}
