package fantom.shoppinglist.activities.shoppingListDetails;

import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import fantom.shoppinglist.activities.shoppingListDetails.interfaces.ShoppingListDetailsInterface;
import fantom.shoppinglist.customViews.ShoppingListItemView;
import fantom.shoppinglist.db.repository.ShoppingListItemRepository;
import fantom.shoppinglist.db.repository.ShoppingListRepository;
import fantom.shoppinglist.model.ShoppingList;
import fantom.shoppinglist.model.ShoppingListItem;

public class ShoppingListDetailsController {
    @Inject
    protected ShoppingListRepository mShoppingListRepository;
    @Inject
    protected ShoppingListItemRepository mShoppingListItemRepository;

    private ShoppingList mShoppingList;
    private List<ShoppingListItem> mShoppingListItemList = new ArrayList<>();

    private ShoppingListDetailsInterface mView;

    @Inject
    public ShoppingListDetailsController() {
    }

    public void onCreate(ShoppingListDetailsInterface shoppingListDetails) {
        mView = shoppingListDetails;
    }

    void createNewList(String title) {
        mShoppingList = new ShoppingList();
        mShoppingList.setListId(UUID.randomUUID());
        mShoppingList.setListTitle(title);
        mShoppingList.setCreateDate(System.currentTimeMillis());
    }

    /**
     * Method called after user selected to save list
     * @param container LinearLayout containing all ShoppingList items
     */
    void updateShoppingList(LinearLayout container) {
        for (int i = 0; i < container.getChildCount(); i++) {
            ShoppingListItemView itemView = (ShoppingListItemView) container.getChildAt(i);
            mShoppingListItemRepository.createOrUpdate(itemView.getShoppingListItem());
        }
        mShoppingListRepository.createOrUpdate(mShoppingList);
        if (mView != null) {
            mView.onListUpdated();
        }
    }

    /**
     * Method to find selected ShoppingList and its items in repository
     * @param listID ID of list
     */
    void getShoppingList(String listID) {
        mShoppingList = mShoppingListRepository.findById(UUID.fromString(listID));
        mShoppingListItemList = mShoppingListItemRepository.findItemsFormList(mShoppingList.getListId());
        if (mView != null) mView.onListLoaded(mShoppingList, mShoppingListItemList);
    }

    /**
     * Method to archive selected ShoppingList
     */
    public void archiveShoppingList() {
        if (mShoppingList != null) {
            mShoppingList.setArchived(true);
            mShoppingListRepository.createOrUpdate(mShoppingList);
        }
        if (mView != null) mView.onListLoaded(mShoppingList, mShoppingListItemList);
    }

    ShoppingList getShoppingList() {
        return mShoppingList;
    }
}
