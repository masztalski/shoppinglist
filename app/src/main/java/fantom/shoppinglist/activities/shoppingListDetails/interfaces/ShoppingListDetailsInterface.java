package fantom.shoppinglist.activities.shoppingListDetails.interfaces;

import java.util.List;

import fantom.shoppinglist.model.ShoppingList;
import fantom.shoppinglist.model.ShoppingListItem;

public interface ShoppingListDetailsInterface {
    void onListUpdated();

    void onListLoaded(ShoppingList shoppingList, List<ShoppingListItem> shoppingListItemList);
}
