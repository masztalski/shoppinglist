package fantom.shoppinglist.activities.shoppingListDetails;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import fantom.shoppinglist.R;
import fantom.shoppinglist.ShoppingListApp;
import fantom.shoppinglist.activities.BaseActivity;
import fantom.shoppinglist.activities.shoppingListDetails.interfaces.ShoppingListDetailsInterface;
import fantom.shoppinglist.customViews.ShoppingListItemView;
import fantom.shoppinglist.databinding.ShoppingListDetailsBinding;
import fantom.shoppinglist.dialogs.ConfirmArchiveDialog;
import fantom.shoppinglist.dialogs.NewShoppingListDialog;
import fantom.shoppinglist.model.ShoppingList;
import fantom.shoppinglist.model.ShoppingListItem;
import fantom.shoppinglist.utils.DateFormatter;
import fantom.shoppinglist.utils.ViewUtils;


public class ShoppingListDetails extends BaseActivity implements ShoppingListDetailsInterface {
    public static final String SHOPPING_LIST_ID = "SHOPPING_LIST_ID";

    private ShoppingListDetailsBinding mBinding;

    @Inject
    protected ShoppingListDetailsController mController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.shopping_list_details);
        setupToolbar();

        ((ShoppingListApp) getApplication()).mApplicationComponent
                .inject(this);

        mController.onCreate(this);

        if (getIntent().hasExtra(SHOPPING_LIST_ID)) {
            mController.getShoppingList(getIntent().getStringExtra(SHOPPING_LIST_ID));
        } else {
            showNewTitleDialog();
        }

        mBinding.addNewItem.setOnClickListener(view -> {
            ShoppingListItemView shoppingListItemView = new ShoppingListItemView(this);
            shoppingListItemView.setListId(mController.getShoppingList().getListId());
            mBinding.container.addView(shoppingListItemView);
            ViewUtils.showKeyboard(this);
        });

        mBinding.saveChanges.setOnClickListener(view -> mController.updateShoppingList(mBinding.container));
    }

    private void showNewTitleDialog() {
        NewShoppingListDialog newShoppingListDialog = new NewShoppingListDialog();
        newShoppingListDialog.setTitleCallback(title -> {
            mBinding.toolbar.toolbar.setTitle(title + " " + DateFormatter.format(System.currentTimeMillis()));
            mController.createNewList(title);
            mBinding.addNewItem.callOnClick();
        });
        newShoppingListDialog.show(getSupportFragmentManager(), NewShoppingListDialog.class.getSimpleName());
    }

    @Override
    public void onListLoaded(ShoppingList shoppingList, List<ShoppingListItem> shoppingListItemList) {
        //noinspection ConstantConditions
        getSupportActionBar().setTitle(shoppingList.getListTitle() + " " + DateFormatter.format(shoppingList.getCreateDate()));
        loadView(shoppingList.isArchived(), shoppingListItemList);
    }

    private void loadView(boolean archived, List<ShoppingListItem> shoppingListItemList) {
        mBinding.container.removeAllViewsInLayout();

        for (ShoppingListItem shoppingListItem : shoppingListItemList) {
            ShoppingListItemView itemView = new ShoppingListItemView(this);
            itemView.setShoppingListItem(shoppingListItem);
            itemView.setEnabled(!archived);
            mBinding.container.addView(itemView);
        }

        if (archived){
            mBinding.addNewItem.setVisibility(View.GONE);
            mBinding.saveChanges.setEnabled(false);
        }
    }

    @Override
    public void onListUpdated() {
        Toast.makeText(this, "List have been saved", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public Toolbar prepareToolbar() {
        return mBinding.toolbar.toolbar;
    }

    @Override
    public boolean shouldDisplayHomeAsUpEnabled() {
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.details_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.archive){
            if (mController.getShoppingList() != null && !mController.getShoppingList().isArchived()) {
                ConfirmArchiveDialog confirmArchiveDialog = new ConfirmArchiveDialog();
                confirmArchiveDialog.setArchiveCallback(() -> mController.archiveShoppingList());
                confirmArchiveDialog.show(getSupportFragmentManager(), ConfirmArchiveDialog.class.getSimpleName());
            } else {
                Toast.makeText(this, "This list is already archived", Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
