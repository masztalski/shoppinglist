package fantom.shoppinglist.activities.shoppingList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import fantom.shoppinglist.activities.shoppingList.interfaces.MainInterface;
import fantom.shoppinglist.db.repository.ShoppingListItemRepository;
import fantom.shoppinglist.db.repository.ShoppingListRepository;
import fantom.shoppinglist.model.ShoppingList;

public class ShoppingListController {
    @Inject
    protected ShoppingListRepository mShoppingListRepository;
    @Inject
    protected ShoppingListItemRepository mShoppingListItemRepository;

    private MainInterface mView;

    private List<ShoppingList> mActiveShoppingLists = new ArrayList<>();
    private List<ShoppingList> mArchivedShoppingLists = new ArrayList<>();

    @Inject
    public ShoppingListController(){}

    void onCreate(MainInterface mainInterface) {
        mView = mainInterface;
    }

    void refreshLists(@ShoppingList.ShopListType int listType) {
        if (listType == ShoppingList.BOTH || listType == ShoppingList.ACTIVE) {
            mActiveShoppingLists = mShoppingListRepository.findByType(ShoppingList.ACTIVE);
        }

        if (listType == ShoppingList.BOTH || listType == ShoppingList.ARCHIVED) {
            mArchivedShoppingLists = mShoppingListRepository.findByType(ShoppingList.ARCHIVED);
        }

        if (isViewPresent())
            mView.onListsRefreshed(listType);
    }

    private boolean isViewPresent() {
        return mView != null;
    }

    List<ShoppingList> getShoppingListByType(@ShoppingList.ShopListType int listType) {
        List<ShoppingList> shoppingLists = listType == ShoppingList.ACTIVE ? mActiveShoppingLists : mArchivedShoppingLists;
        Collections.sort(shoppingLists, (l1, l2) -> ((Long)(l2.getCreateDate() - l1.getCreateDate())).intValue());
        return shoppingLists;
    }

    void archiveShoppingList(ShoppingList shoppingList){
        shoppingList.setArchived(true);
        mShoppingListRepository.update(shoppingList);
        refreshLists(ShoppingList.ACTIVE);
        refreshLists(ShoppingList.ARCHIVED);
    }
}
