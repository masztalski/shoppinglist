package fantom.shoppinglist.activities.shoppingList.interfaces;

import java.util.List;

import fantom.shoppinglist.model.ShoppingList;

public interface MainInterface {
    List<ShoppingList> getShoppingListsByType(@ShoppingList.ShopListType int listType);

    void onListsRefreshed(@ShoppingList.ShopListType int listType);
}
