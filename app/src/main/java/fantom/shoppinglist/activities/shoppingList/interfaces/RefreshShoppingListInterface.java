package fantom.shoppinglist.activities.shoppingList.interfaces;

import fantom.shoppinglist.model.ShoppingList;

public interface RefreshShoppingListInterface {
    void onListRefreshed(@ShoppingList.ShopListType int listType);
}
