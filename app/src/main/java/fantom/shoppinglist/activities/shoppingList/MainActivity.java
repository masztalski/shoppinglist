package fantom.shoppinglist.activities.shoppingList;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import fantom.shoppinglist.R;
import fantom.shoppinglist.ShoppingListApp;
import fantom.shoppinglist.activities.BaseActivity;
import fantom.shoppinglist.activities.shoppingList.interfaces.MainInterface;
import fantom.shoppinglist.activities.shoppingListDetails.ShoppingListDetails;
import fantom.shoppinglist.adapters.ListsPagerAdapter;
import fantom.shoppinglist.adapters.ShoppingListAdapter;
import fantom.shoppinglist.databinding.ActivityMenuBinding;
import fantom.shoppinglist.dialogs.ConfirmArchiveDialog;
import fantom.shoppinglist.model.ShoppingList;

public class MainActivity extends BaseActivity implements MainInterface, ShoppingListAdapter.OnShoppingListClickCallback {
    @Inject
    protected ShoppingListController mController;

    private ListsPagerAdapter mListsPagerAdapter;

    private ActivityMenuBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_menu);
        setupViewPager();
        setupToolbar();

        ((ShoppingListApp) getApplication()).mApplicationComponent
                .inject(this);

        mController.onCreate(this);
        mController.refreshLists(ShoppingList.BOTH);

        mBinding.newListBtn.setOnClickListener(view -> {
            Intent intent = new Intent(this, ShoppingListDetails.class);
            startActivity(intent);
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mController.refreshLists(ShoppingList.ACTIVE);
        mController.refreshLists(ShoppingList.ARCHIVED);
    }

    private void setupViewPager() {
        mListsPagerAdapter = new ListsPagerAdapter(getSupportFragmentManager());
        mBinding.viewpager.setAdapter(mListsPagerAdapter);
        mBinding.viewpager.setOffscreenPageLimit(mListsPagerAdapter.getCount() - 1);
        mBinding.tablayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mBinding.tablayout.setTabMode(TabLayout.MODE_FIXED);
        mBinding.tablayout.setupWithViewPager(mBinding.viewpager);
    }

    /**
     * Method delegating to controller query about list of ShoppingLists according to its type (Active/Archived)
     *
     * @param listType type of list
     * @return list of ShoppingLists of type listType
     */
    @Override
    public List<ShoppingList> getShoppingListsByType(@ShoppingList.ShopListType int listType) {
        return mController.getShoppingListByType(listType);
    }

    /**
     * Method called after new lists have been loaded. Refereshes view of selected list
     *
     * @param listType type of list to refreshed
     */
    @Override
    public void onListsRefreshed(@ShoppingList.ShopListType int listType) {
        Log.i("MainActivity", "Refresh list: " + listType);
        mListsPagerAdapter.notifyDataChanged(listType);
    }

    /**
     * Method called after selecting one of ShoppingLists
     *
     * @param shoppingList clicked ShoppingList
     */
    @Override
    public void onItemClick(ShoppingList shoppingList) {
        Intent intent = new Intent(this, ShoppingListDetails.class);
        intent.putExtra(ShoppingListDetails.SHOPPING_LIST_ID, shoppingList.getListId().toString());
        startActivity(intent);
    }

    /**
     * Method called after long click on one of ShoppingLists.
     * Used to archive list
     *
     * @param shoppingList clicked ShoppingList
     */
    @Override
    public void onItemLongClick(ShoppingList shoppingList) {
        ConfirmArchiveDialog confirmArchiveDialog = new ConfirmArchiveDialog();
        confirmArchiveDialog.setArchiveCallback(() -> {
            mController.archiveShoppingList(shoppingList);
            onListsRefreshed(ShoppingList.ACTIVE);
            onListsRefreshed(ShoppingList.ARCHIVED);
        });
        confirmArchiveDialog.show(getSupportFragmentManager(), ConfirmArchiveDialog.class.getSimpleName());
    }

    @Override
    public Toolbar prepareToolbar() {
        return mBinding.toolbar.toolbar;
    }

    @Override
    public boolean shouldDisplayHomeAsUpEnabled() {
        return false;
    }
}
