package fantom.shoppinglist.activities.shoppingList;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fantom.shoppinglist.R;
import fantom.shoppinglist.activities.shoppingList.interfaces.MainInterface;
import fantom.shoppinglist.activities.shoppingList.interfaces.RefreshShoppingListInterface;
import fantom.shoppinglist.adapters.ShoppingListAdapter;
import fantom.shoppinglist.databinding.ShoppingListBinding;
import fantom.shoppinglist.model.ShoppingList;


public class ShoppingListFragment extends Fragment implements RefreshShoppingListInterface {
    public static final String LIST_TYPE = "TYPE_OF_LIST";

    private @ShoppingList.ShopListType int mListType;
    private ShoppingListBinding mBinding;
    private ShoppingListAdapter mAdapter = new ShoppingListAdapter();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(LIST_TYPE)) {
            //noinspection WrongConstant
            mListType = getArguments().getInt(LIST_TYPE, ShoppingList.ACTIVE);
        }
    }

    /**
     * Method called from PagerAdapter to referesh list on view
     *
     * @param listType type of list
     */
    @Override
    public void onListRefreshed(@ShoppingList.ShopListType int listType) {
        if (getParentView() != null && (listType == ShoppingList.BOTH || mListType == listType)) {
            refreshShoppingLists(getParentView().getShoppingListsByType(listType));
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.shopping_list, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        lm.setAutoMeasureEnabled(false);
        mBinding.recyclerView.setLayoutManager(lm);
        mBinding.recyclerView.setAdapter(mAdapter);
        mAdapter.mListType = mListType;

        mAdapter.setOnShoppingListClickCallback(getOnItemClickListener());
        refreshShoppingLists(getParentView().getShoppingListsByType(mListType));
    }


    /**
     * Method called after list of ShoppingList loaded
     *
     * @param shoppingLists loaded list of ShoppingList
     */
    private void refreshShoppingLists(@NonNull List<ShoppingList> shoppingLists) {
        mAdapter.setShoppingLists(shoppingLists);
    }

    /**
     * Method helps to talk between two ShoppingListFragment by MainActvity
     *
     * @return interface of MainActivity
     */
    private MainInterface getParentView() {
        return (MainInterface) getActivity();
    }

    /**
     * Method delegating actions with selected ShoppingList to MainActivity
     *
     * @return callback to use for making actions after click on selected ShoppingList
     */
    private ShoppingListAdapter.OnShoppingListClickCallback getOnItemClickListener() {
        return (ShoppingListAdapter.OnShoppingListClickCallback) getActivity();
    }
}
