package fantom.shoppinglist.di;

import javax.inject.Singleton;

import dagger.Component;
import fantom.shoppinglist.activities.shoppingList.MainActivity;
import fantom.shoppinglist.activities.shoppingListDetails.ShoppingListDetails;
import fantom.shoppinglist.di.modules.ApplicationModule;
import fantom.shoppinglist.di.modules.RepositoryModule;

@Singleton
@Component(modules = {ApplicationModule.class, RepositoryModule.class})
public interface ApplicationComponent {
    void inject(MainActivity activity);

    void inject(ShoppingListDetails shoppingListDetails);
}
