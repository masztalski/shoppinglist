package fantom.shoppinglist.di.modules;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Singleton
@Module
public class ApplicationModule {
    private Application mApp;

    public ApplicationModule(Application shoppingListApp) {
        this.mApp = shoppingListApp;
    }

    @Provides
    @Singleton
    Application providesApp(){
        return mApp;
    }
}
