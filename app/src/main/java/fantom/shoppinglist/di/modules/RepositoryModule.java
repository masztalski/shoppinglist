package fantom.shoppinglist.di.modules;

import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import fantom.shoppinglist.db.DatabaseHelper;
import fantom.shoppinglist.db.impl.ShoppingListItemSQLImpl;
import fantom.shoppinglist.db.impl.ShoppingListSQLImpl;
import fantom.shoppinglist.db.repository.ShoppingListItemRepository;
import fantom.shoppinglist.db.repository.ShoppingListRepository;

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    ConnectionSource providesConnectionSource(Application app) {
        return new AndroidConnectionSource(new DatabaseHelper(app.getApplicationContext()));
    }

    @Provides
    @Singleton
    ShoppingListRepository providesShoppingListRepo(ConnectionSource connectionSource){
        return new ShoppingListSQLImpl(connectionSource);
    }

    @Provides
    @Singleton
    ShoppingListItemRepository providesShoppingListItemRepo(ConnectionSource connectionSource){
        return new ShoppingListItemSQLImpl(connectionSource);
    }
}
