package fantom.shoppinglist.db.repository;

import java.util.List;
import java.util.UUID;

import fantom.shoppinglist.model.ShoppingList;

public interface ShoppingListRepository extends BaseRepository<ShoppingList, UUID> {
    List<ShoppingList> findByType(@ShoppingList.ShopListType int listType);
}
