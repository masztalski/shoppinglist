package fantom.shoppinglist.db.repository;

import java.util.List;

/**
 * Basic operations on database
 * @param <T> type of model
 * @param <I> type of ID for T model
 */
public interface BaseRepository<T, I> {
    T findById(I id);
    List<T> findAll();

    boolean delete(T object);

    boolean create(T r);

    boolean update(T r);

    boolean createOrUpdate(T r);

    boolean isEmpty();

    boolean deleteAll();

    long count();
}
