package fantom.shoppinglist.db.repository;

import java.util.List;
import java.util.UUID;

import fantom.shoppinglist.model.ShoppingListItem;

public interface ShoppingListItemRepository extends BaseRepository<ShoppingListItem, UUID> {
    List<ShoppingListItem> findItemsFormList(UUID listId);
}
