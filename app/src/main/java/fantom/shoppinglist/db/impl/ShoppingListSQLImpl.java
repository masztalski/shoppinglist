package fantom.shoppinglist.db.impl;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import fantom.shoppinglist.db.repository.ShoppingListRepository;
import fantom.shoppinglist.model.ShoppingList;


public class ShoppingListSQLImpl extends BaseSQLImpl<ShoppingList, UUID> implements ShoppingListRepository {
    public ShoppingListSQLImpl(ConnectionSource connectionSource) {
        try {
            mDao = DaoManager.createDao(connectionSource, ShoppingList.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<ShoppingList> findByType(@ShoppingList.ShopListType int listType) {
        try {
            Where<ShoppingList, UUID> where = mDao.queryBuilder().where();
            where.eq("mIsArchived", listType == ShoppingList.ARCHIVED);
            return mDao.query(where.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}
