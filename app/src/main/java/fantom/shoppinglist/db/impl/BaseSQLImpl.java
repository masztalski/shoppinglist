package fantom.shoppinglist.db.impl;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import fantom.shoppinglist.db.repository.BaseRepository;

public class BaseSQLImpl<T, I> implements BaseRepository<T, I> {
    protected Dao<T, I> mDao;

    @Override
    public T findById(I id) {
        try {
            return id != null ? mDao.queryForId(id) : null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<T> findAll() {
        try {
            return mDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @Override
    public boolean delete(T object) {
        return false;
    }

    @Override
    public boolean create(T r) {
        try {
            return mDao.create(r) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(T r) {
        try {
            return mDao.update(r) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean createOrUpdate(T r) {
        try {
            Dao.CreateOrUpdateStatus status = mDao.createOrUpdate(r);
            return status.isUpdated() || status.isCreated();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean isEmpty() {
        return count() == 0;
    }

    @Override
    public boolean deleteAll() {
        if (!isEmpty()) {
            try {
                return mDao.deleteBuilder().delete() > 0;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public long count() {
        try {
            return mDao.countOf();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
