package fantom.shoppinglist.db.impl;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import fantom.shoppinglist.db.repository.ShoppingListItemRepository;
import fantom.shoppinglist.model.ShoppingListItem;


public class ShoppingListItemSQLImpl extends BaseSQLImpl<ShoppingListItem, UUID> implements ShoppingListItemRepository {

    public ShoppingListItemSQLImpl(ConnectionSource connectionSource){
        try {
            mDao = DaoManager.createDao(connectionSource, ShoppingListItem.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<ShoppingListItem> findItemsFormList(UUID listId) {
        try {
            Where<ShoppingListItem, UUID> where = mDao.queryBuilder().where();
            where.eq("mListId", listId);
            return mDao.query(where.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}
