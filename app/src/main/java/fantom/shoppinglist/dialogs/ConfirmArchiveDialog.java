package fantom.shoppinglist.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;


public class ConfirmArchiveDialog extends DialogFragment {

    private ConfirmArchiveCallback mArchiveCallback;

    public interface ConfirmArchiveCallback {
        void onArchive();
    }

    public void setArchiveCallback(ConfirmArchiveCallback archiveCallback) {
        mArchiveCallback = archiveCallback;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle("Archive shopping list")
                .setMessage("Do you want archive this shopping list?")
                .setPositiveButton("Yes", (dialogInterface, i) -> {
                    if (mArchiveCallback != null)
                        mArchiveCallback.onArchive();
                    dialogInterface.dismiss();
                })
                .setNegativeButton("No", (dialogInterface, i) -> dialogInterface.dismiss()).create();
    }
}
