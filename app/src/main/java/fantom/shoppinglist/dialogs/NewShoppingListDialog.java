package fantom.shoppinglist.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;

import fantom.shoppinglist.R;
import fantom.shoppinglist.databinding.NewShoppingListBinding;
import fantom.shoppinglist.utils.ViewUtils;


public class NewShoppingListDialog extends AppCompatDialogFragment {

    private NewShoppingListBinding mBinding;
    private TitleCallback mCallback;

    public interface TitleCallback {
        void onTitleInput(String title);
    }

    public void setTitleCallback(TitleCallback callback) {
        mCallback = callback;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.new_shopping_list, null, false);
        mBinding.title.requestFocus();

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setView(mBinding.getRoot())
                .setTitle("Set shopping list title")
                .setPositiveButton("OK", null)
                .create();

        alertDialog.setOnShowListener(dialog -> alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(v -> {
            if (mCallback != null)
                mCallback.onTitleInput(mBinding.title.getText() != null ? mBinding.title.getText().toString() : "");
            getDialog().dismiss();
        }));

        ViewUtils.showKeyboardDialog(alertDialog);

        return alertDialog;
    }


}
