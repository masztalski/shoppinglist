package fantom.shoppinglist.adapters;

import com.annimon.stream.Stream;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import fantom.shoppinglist.activities.shoppingList.ShoppingListFragment;
import fantom.shoppinglist.activities.shoppingList.interfaces.RefreshShoppingListInterface;
import fantom.shoppinglist.model.ShoppingList;

public class ListsPagerAdapter extends FragmentPagerAdapter {
    private List<RefreshShoppingListInterface> mFragmentList;
    private String[] mTabTitles = {"Active", "Archived"};
    private int[] mTabTypes = {ShoppingList.ACTIVE, ShoppingList.ARCHIVED};

    public ListsPagerAdapter(FragmentManager fm) {
        super(fm);
        prepare();
    }

    private void prepare() {
        this.mFragmentList = new ArrayList<>();
        for (int i = 0; i < mTabTitles.length; i++) {
            Fragment fragment = new ShoppingListFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(ShoppingListFragment.LIST_TYPE, mTabTypes[i]);
            fragment.setArguments(bundle);

            mFragmentList.add((RefreshShoppingListInterface) fragment);
        }
    }

    @Override
    public int getCount() {
        return mTabTitles.length;
    }

    @Override
    public Fragment getItem(int position) {
        return (Fragment) mFragmentList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabTitles[position];
    }

    public void notifyDataChanged(@ShoppingList.ShopListType int shopListType) {
        Stream.of(mFragmentList).forEach(value -> value.onListRefreshed(shopListType));
    }
}
