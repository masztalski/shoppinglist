package fantom.shoppinglist.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fantom.shoppinglist.R;
import fantom.shoppinglist.databinding.ShoppingListItemBinding;
import fantom.shoppinglist.model.ShoppingList;
import fantom.shoppinglist.utils.DateFormatter;

public class ShoppingListAdapter extends RecyclerView.Adapter<ShoppingListAdapter.ShoppingListViewHolder> {
    public @ShoppingList.ShopListType int mListType;

    private OnShoppingListClickCallback mClickCallback;
    private List<ShoppingList> mItemList;

    public void setOnShoppingListClickCallback(OnShoppingListClickCallback onItemClickListener) {
        mClickCallback = onItemClickListener;
    }

    public void setShoppingLists(List<ShoppingList> shoppingLists) {
        mItemList = shoppingLists;
        notifyDataSetChanged();
    }

    public interface OnShoppingListClickCallback {
        void onItemClick(ShoppingList shoppingList);

        void onItemLongClick(ShoppingList shoppingList);
    }

    @Override
    public ShoppingListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ShoppingListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ShoppingListViewHolder holder, int position) {
        ShoppingList shoppingList = mItemList.get(position);
        holder.mBinding.title.setText(shoppingList.getListTitle());
        holder.mBinding.date.setText(DateFormatter.format(shoppingList.getCreateDate()));
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }


    class ShoppingListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        ShoppingListItemBinding mBinding;

        ShoppingListViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
            itemView.setOnClickListener(this);
            if (mListType == ShoppingList.ACTIVE) itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickCallback != null)
                mClickCallback.onItemClick(mItemList.get(getAdapterPosition()));
        }

        @Override
        public boolean onLongClick(View view) {
            if (mClickCallback != null) {
                mClickCallback.onItemLongClick(mItemList.get(getAdapterPosition()));
                return true;
            }
            return false;
        }
    }
}
