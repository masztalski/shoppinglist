package fantom.shoppinglist.customViews;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import java.util.UUID;

import fantom.shoppinglist.R;
import fantom.shoppinglist.databinding.ViewListItemBinding;
import fantom.shoppinglist.model.ShoppingListItem;
import fantom.shoppinglist.utils.ViewUtils;

/**
 * CustomView made of CheckBox and EditText used to handle single item from ShoppingList
 */
public class ShoppingListItemView extends LinearLayout {
    private ViewListItemBinding mBinding;

    private ShoppingListItem mShoppingListItem;

    public ShoppingListItemView(Context context) {
        super(context);
        init();
    }

    public ShoppingListItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ShoppingListItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setOrientation(HORIZONTAL);
        setPadding(16, 16, 16, 16);
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.view_list_item, this, true);
        mBinding.itemName.requestFocus();
    }

    public ShoppingListItem getShoppingListItem() {
        mShoppingListItem.setItemName(mBinding.itemName.getText().toString());
        mShoppingListItem.setBought(mBinding.isBuyed.isChecked());

        return mShoppingListItem;
    }

    /**
     * Method to identify parent ShoppingList
     * @param shoppingListID ID of parent ShoppingList
     */
    public void setListId(UUID shoppingListID){
        if (mShoppingListItem == null) {
            mShoppingListItem = new ShoppingListItem();
        }
        mShoppingListItem.setListId(shoppingListID);
    }

    @Override
    public void setEnabled(boolean enabled) {
        mBinding.isBuyed.setEnabled(enabled);
        mBinding.itemName.setEnabled(enabled);
        super.setEnabled(enabled);
    }

    public void setShoppingListItem(ShoppingListItem shoppingListItem) {
        mShoppingListItem = shoppingListItem;
        mBinding.itemName.setText(shoppingListItem.getItemName());
        mBinding.isBuyed.setChecked(shoppingListItem.isBought());
        mBinding.itemName.setEnabled(!shoppingListItem.isBought());
    }
}
