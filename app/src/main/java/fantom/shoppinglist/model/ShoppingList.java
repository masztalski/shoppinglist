package fantom.shoppinglist.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.UUID;

@DatabaseTable(tableName = "shopping_list")
public class ShoppingList {
    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
    private UUID mListId;
    @DatabaseField
    private String mListTitle;
    @DatabaseField
    private long mCreateDate;
    @DatabaseField
    private boolean mIsArchived;

    @Retention(RetentionPolicy.RUNTIME)
    @IntDef({ACTIVE, ARCHIVED, BOTH})
    public @interface ShopListType {
    }

    public static final int ACTIVE = 0;
    public static final int ARCHIVED = 1;
    public static final int BOTH = 2;

    public ShoppingList() {
    }

    public ShoppingList(UUID listId, String listTitle, long createDate) {
        mListId = listId;
        mListTitle = listTitle;
        mCreateDate = createDate;
        mIsArchived = false;
    }

    public UUID getListId() {
        return mListId;
    }

    public void setListId(UUID listId) {
        mListId = listId;
    }

    public String getListTitle() {
        return mListTitle;
    }

    public void setListTitle(String listTitle) {
        mListTitle = listTitle;
    }

    public long getCreateDate() {
        return mCreateDate;
    }

    public void setCreateDate(long createDate) {
        mCreateDate = createDate;
    }

    public boolean isArchived() {
        return mIsArchived;
    }

    public void setArchived(boolean archived) {
        mIsArchived = archived;
    }

}
