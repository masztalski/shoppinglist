package fantom.shoppinglist.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.UUID;

@DatabaseTable(tableName = "listItem")
public class ShoppingListItem {
    @DatabaseField(generatedId = true)
    private UUID mItemId;
    @DatabaseField
    private String mItemName;
    @DatabaseField
    private UUID mListId; //ID of connected list
    @DatabaseField
    private boolean mIsBought;

    public ShoppingListItem() {
    }

    public ShoppingListItem(String itemName, UUID listId) {
        mItemName = itemName;
        mListId = listId;
        mIsBought = false;
    }

    public UUID getItemId() {
        return mItemId;
    }

    public void setItemId(UUID itemId) {
        mItemId = itemId;
    }

    public String getItemName() {
        return mItemName;
    }

    public void setItemName(String itemName) {
        mItemName = itemName;
    }

    public UUID getListId() {
        return mListId;
    }

    public void setListId(UUID listId) {
        mListId = listId;
    }

    public boolean isBought() {
        return mIsBought;
    }

    public void setBought(boolean bought) {
        mIsBought = bought;
    }
}
