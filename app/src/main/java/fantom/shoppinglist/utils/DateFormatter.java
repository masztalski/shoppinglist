package fantom.shoppinglist.utils;

import java.text.SimpleDateFormat;

/**
 * Util class to format date from long to String
 */
public final class DateFormatter {
    private static SimpleDateFormat mSimpleDate = new SimpleDateFormat("dd/MM/yyyy");

    public static String format(long date) {
        return mSimpleDate.format(date);
    }
}
